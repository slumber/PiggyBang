
![img](gameplay/piggy/cochon_01.png)

*A 2 player game opposing a pig and a trader.*

## Rules

The trader need to fill the pig with coins to win.
The pig need to empty himself with hamers hit.

## Inputs

Trader:
- move left : Q
- move right : D
- fire: Left Mouse Button
- target: Mouse Direction

PIG:
- move left : LEFT ARROW
- move right : RIGHT ARROW
- jump : Space

## Credit:

- Laure LS
- Swann M
- Samuel B
- Etienne S
- Valentin G
- Apoil ASSO