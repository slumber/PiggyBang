extends RigidBody2D

# Declare member variables here. Examples:
var timeSinceStart = 0
export var timeToKill = 3

var hitSound1 = load("res://sounds/Rebond1.wav")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start(timeToKill)
	$Timer.connect("timeout", self, "queue_free")
	connect("body_entered",self,"_on_coin_body_entered")
	pass
#	$CollisionShape2D.connect("body_shape_entered", self, "mapCollision")

func _on_coin_body_entered(body):
	if body.is_in_group("piggy"):
		body.on_collision()
		queue_free()
	else:
		$AudioStreamPlayer2D.stream = hitSound1
		$AudioStreamPlayer2D.play()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timeSinceStart += delta
	if timeSinceStart > float(timeToKill)/2:
		$Sprite.modulate = Color(1, 1, 1, int(timeSinceStart<timeToKill)*(timeToKill*1.5-timeSinceStart)/timeToKill)
