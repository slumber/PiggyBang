extends Node2D

var isFalling = false
var isAscending = false
var velocity = 0
var hitCount = 0
var maxCollisionCount = 3
var ascensionSpeed = 1
export var acceleration = 9.81
export var fallSpeed = 0.5
export var numberOfDroppedCoins = 2
export var hammerMaxAngle = 78.2

var pigHitSound = load("res://sounds/MarteauCochon.wav")
var groundHitSound = load("res://sounds/MarteauSol.wav")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if isFalling:
		velocity += sin(rotation)*delta*acceleration
		rotate(velocity*delta)
		if abs(rotation) > hammerMaxAngle / 180 * PI:
			$AudioStreamPlayer2D.stream = groundHitSound
			$AudioStreamPlayer2D.play()
			isFalling = false
			isAscending = true
			rotation = -hammerMaxAngle / 180 * PI
		if rotation > 0:
			isFalling = false
			rotation = 0
	else:
		if isAscending:
			rotate(ascensionSpeed*delta)
			if rotation > 0:
				isAscending = false
				rotation = 0
				hitCount = 0

func makeFall(body):
	if !isFalling && !isAscending && body.is_in_group("piggy"):
		isFalling = true
		velocity = -fallSpeed

func onCollision(body):
	if isFalling:
		if body.is_in_group("piggy"):
			$AudioStreamPlayer2D.stream = pigHitSound
			$AudioStreamPlayer2D.play()
			for i in range(numberOfDroppedCoins):
				body.call_deferred("drop_gold")
			velocity = -velocity
			hitCount += 1
			if hitCount >= maxCollisionCount:
				isFalling = false
				isAscending = true