extends Node2D

var currentNumber = 0
var processFunc = false
var startTimer = 0
var startingPos

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("First"):
		if currentNumber == 0 || currentNumber == 1:
			currentNumber = 1
		else:
			currentNumber = 0
	if Input.is_action_pressed("Second"):
		if currentNumber == 1 || currentNumber == 2:
			currentNumber = 2
		else:
			currentNumber = 0
	if Input.is_action_pressed("Third"):
		if currentNumber == 2 || currentNumber == 3:
			currentNumber = 3
		else:
			currentNumber = 0
	if Input.is_action_pressed("Fourth"):
		if currentNumber == 3:
			processFunc = true
			startingPos = $Camera2D.position
		else:
			currentNumber = 0
	if processFunc:
		startTimer += delta
		if startTimer < 2:
			$Camera2D.position = startTimer*0.5*(Vector2(6800, 6800)+Vector2(1920/8, 1080/8))+(1-startTimer*0.5)*startingPos
			$Camera2D.rotation_degrees = startTimer*180
		else:
			$Camera2D.rotation_degrees = 0
			$Camera2D.position = Vector2(6800, 6800)+Vector2(1920/8, 1080/8)
			processFunc = false
			print($Camera2D.position)

func _end_game():
	pass